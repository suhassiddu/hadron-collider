Here we will train a classifier to identify type of a particle. 
There are six particle types: electron, proton, muon, kaon, pion and ghost. 
Ghost is a particle with other type than the first five or a detector noise.
Different particle types remain different responses in the detector systems or subdetectors. 
Thre are five systems: tracking system, ring imaging Cherenkov detector (RICH), electromagnetic and hadron calorimeters, 
and muon system. You task is to identify a particle type using the responses in the detector systems. 

## The data files:

https://drive.google.com/drive/folders/1UfscKmN6T6PXGXEwsJFepy_NjMM5cKr_?usp=sharing