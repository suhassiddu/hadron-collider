# Hadron-Collider

![pid](./Particle_identification/pic/pid.jpg)
## Measuring the Z boson mass

Here we are analysing the mass spectrum of particle decaying into muon-antimuon (dimuon) pairs and figure out the mass of Z boson. The mass of Z boson can be looked up at the PDG and measuring.

We can find the CSV file with dimuon events. We can make a histogram of mass distribution, it can fit a mixture of parametrised signal and background distributions into the shape of the histogram. The signal corresponds to a peak-like shape. The background for simplicity we estimate as a flat level. The parameters of the mixture will be the mass of Z boson and other estimations of the significance and uncertainty of the measurement.

We fit the mixture of 1) Gaussian distribution and 2) flat distribution using scikit-optimise toolkit as it is described in the notebook here. Thus we obtain parameters of those distributions: 1) centre of the Gaussian, it's peak height, and deviation; 2) level of the background.

The dataset is taken from CERN opendata portal http://opendata.cern.ch/record/545

## Identify type of a particle

Here we will train a classifier to identify type of a particle. 
There are six particle types: electron, proton, muon, kaon, pion and ghost. 
Ghost is a particle with other type than the first five or a detector noise.
Different particle types remain different responses in the detector systems or subdetectors. 
Thre are five systems: tracking system, ring imaging Cherenkov detector (RICH), electromagnetic and hadron calorimeters, 
and muon system. You task is to identify a particle type using the responses in the detector systems. 

### The data files:

https://drive.google.com/drive/folders/1UfscKmN6T6PXGXEwsJFepy_NjMM5cKr_?usp=sharing

## Detector Optimization

Here we will optimize a simple tracking system using Bayesian optimization with Gaussian processes. 
The same method can be used for more complex systems optimization.

## Search for Rare Decay

Data files you will find here https://drive.google.com/drive/folders/1UfscKmN6T6PXGXEwsJFepy_NjMM5cKr_?usp=sharing

Here we will design prediction model that will give best score (ROC AUC) and will meet the constraints of 

- similar performance on simulated and real data (agreement check)
- decorrelation with the mass (correlation check)

## Search for Electromagnetic Showers

Here we will design a prediction model that is able to discriminate the basetracks beloning to the electromagnetic showers from the background basetracks. The goal is to get the best possible score (ROC AUC). 

### The data files:

https://drive.google.com/drive/folders/1UfscKmN6T6PXGXEwsJFepy_NjMM5cKr_?usp=sharing