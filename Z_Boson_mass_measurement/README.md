Here we are analysing the mass spectrum of particle decaying into muon-antimuon (dimuon) pairs and figure out the mass of Z boson. The mass of Z boson can be looked up at the PDG and measuring.

We can find the CSV file with dimuon events. We can make a histogram of mass distribution, it can fit a mixture of parametrised signal and background distributions into the shape of the histogram. The signal corresponds to a peak-like shape. The background for simplicity we estimate as a flat level. The parameters of the mixture will be the mass of Z boson and other estimations of the significance and uncertainty of the measurement.

We fit the mixture of 1) Gaussian distribution and 2) flat distribution using scikit-optimise toolkit as it is described in the notebook here. Thus we obtain parameters of those distributions: 1) centre of the Gaussian, it's peak height, and deviation; 2) level of the background.

The dataset is taken from CERN opendata portal http://opendata.cern.ch/record/545