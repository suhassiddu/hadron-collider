Here we will design a prediction model that is able to discriminate the basetracks beloning to the electromagnetic showers from the background basetracks. The goal is to get the best possible score (ROC AUC). 

## The data files:

https://drive.google.com/drive/folders/1UfscKmN6T6PXGXEwsJFepy_NjMM5cKr_?usp=sharing

training.tgz - archive with brick volumes filled with labeled basetracks
test.h5.gz - volume with basetracks you have to discriminate

Each BaseTrack (BT) is described by:

- Coordinates: (X, Y, Z)

- Angles in the brick-frame: (TX, TY)

After the 'add_neighbours' we add dX,dY,dZ,dTX,dTY (distance from the origin) and group close tracks from neighbour plates into pairs.
